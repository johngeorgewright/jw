<?php

namespace jw\exception;

use jw\Base as JwBase;
use Exception;
use jw\response\http\Html;

class Display extends JwBase
{
  protected $exception;

  public function __construct(Exception $e)
  {
    $this->exception = $e;
  }
  
  public static function handler(Exception $e)
  {
    $display = new self($e);
    echo $display->render();
    exit;
  }

  public function render($html=false)
  {
    $html ? $this->renderHtml() : debug_print_backtrace();
  }
  
  public function renderHtml()
  {
    $content  = '<h1>'.$this->exception->getMessage().'</h1>';
    $content .= '<ol>';
    foreach ($this->exception->getTrace() as $trace)
    {
      $args = array();

      foreach ($trace['args'] as $arg)
      {
        switch (gettype($arg))
        {
          case 'array':
            $args[] = '<em>array</em>';
            break;

          case 'boolean':
            $args[] = $arg ? 'true' : 'false';
            break;

          case 'double':
          case 'integer':
            $args[] = $arg;
            break;

          case 'object':
            $args[] = '<em>'.get_class($arg).'</em>';
            break;

          case 'string':
            $args[] = "\"$arg\"";
            break;

          case 'resource':
            $args[] = '<em>resource</em>';
            break;

          case 'NULL':
            $args[] = '<em>NULL</em>';
            break;

          default:
            $args[] = '<em>'.gettype($arg).'</em> '.$arg;
            break;
        }
      }

      $content .= sprintf(
        '<li>%s%s%s(%s) called at [%s:%d]</li>', 
        $trace['class'],
        $trace['type'],
        $trace['function'],
        implode(', ', $args),
        $trace['file'],
        $trace['line']
      );
    }
    
    $content .= '</ol>';
    
    echo new Html($content, 500, $this->exception->getMessage());
  }
}

