<?php

namespace jw\exception;

class Error404 extends Base
{
  protected $code = 404;

  public function __construct()
  {
    parent::__construct('Not Found');
  }
}

