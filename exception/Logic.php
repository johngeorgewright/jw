<?php

namespace jw\exception;

use LogicException;

class Logic extends LogicException
{
  protected $code = 500;
}

