<?php

namespace jw\exception;

use Exception;

/**
 * A global exception.
 *
 * @author John Wright <johngeorge.wright@gmail.com>
 * @package jw
 */
class Base extends Exception
{
  protected $code = 500;
}

