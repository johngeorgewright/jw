<?php

namespace jw\singleton;

abstract class Base extends \jw\Base
{
  private static $instance;

  public static function getInstance()
  {
    if (empty(self::$instance))
    {
      $class = get_called_class();
      self::$instance = new $class();
    }
    return self::$instance;
  }
}

