<?php

namespace jw\view;

use jw\controller\Base as Controller;
use ReflectionClass;
use InvalidArgumentException;

/**
 * The view abstract for the MVC.
 *
 * @author John Wright <johngeorge.wright@gmail.com>
 * @package jw
 */
abstract class Base extends \jw\Base
{
  /**
   * The template class.
   *
   * @var ReflectionClass
   */
  private $template_class;

  /**
   * The controller object.
   *
   * @var jw\controller\Base
   */
  protected $controller;

  /**
   * A template class is passed during construction.
   *
   * @param string $template_class
   * @throws jw\Exception
   */
  public function __construct(Controller $controller, $template_class)
  {
    $this->controller     = $controller;
    $this->template_class = new ReflectionClass($template_class);

    if (!$this->template_class->isSubclassOf('jw\view\template\Base'))
    {
      throw new InvalidArgumentException("Class \"{$this->template_class->getName()}\" is not an instance of jw\\view\\Template");
    }

    parent::__construct();
  }

  /**
   * Renders a template using the configured templating system.
   *
   * @param string $template_path
   * @param mixed[] $params
   * @return string
   */
  public function render($template_path, array $params=array())
  {
    return $this
      ->template_class
      ->newInstance($this->controller)
      ->render($template_path, $params);
  }
}

