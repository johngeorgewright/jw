<?php

namespace jw\view;

use jw\mixin\Base as Mixin;
use jw\response\http\Html;

class HttpShortcuts extends Mixin
{
  public function renderTemplate($template, array $vars=array())
  {
    return new Html($this->host->render($template, $vars));
  }
}

