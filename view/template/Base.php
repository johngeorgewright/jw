<?php

namespace jw\view\template;

use jw\Base as JwBase;
use jw\controller\Base as Controller;

/**
 * The view templating abstract.
 * 
 * @author John Wright <johngeorge.wright@gmail.com>
 * @package jw.view.template
 */
abstract class Base extends JwBase
{
  /**
   * A controller.
   * 
   * @var jw\controller\Base
   */
  protected $controller;
  
  /**
   * Pass a controller into the templating system when constructing.
   * 
   * @param Controller $controller 
   */
  public function __construct(Controller $controller)
  {
    $this->controller = $controller;
  }

  /**
   * Renders the view and returns it as a string.
   * 
   * @param string $path
   * @param mixed $params
   * @return string
   */
  abstract public function render($path, array $params);
}

