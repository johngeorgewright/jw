<?php

namespace jw\view\template;

use jw\controller\Base as Controller;
use Configuration;
use jw\exception\Logic as LogicException;
use jw\exception\Base as Exception;

/**
 * A PHP template style.
 * 
 * @author John Wright <johngeorge.wright@gmail.com>
 * @package jw.view.template
 */
class PHP extends Base
{
  /**
   * An extension.
   * 
   * @var string
   */
  protected $extension;
  
  /**
   * Blocks' contents
   * 
   * @var string[]
   */
  protected $blocks=array();
  
  /**
   * The current block that's being rendered.
   * 
   * @var string
   */
  protected $current_block;

  /**
   * @param jw\controller\Base $controller
   * @param string[] $blocks Used internally only.
   */
  public function __construct(Controller $controller, array $blocks=array())
  {
    parent::__construct($controller);
    $this->blocks = $blocks;
  }

  /**
   * Either set a block's content or start capturing a block's content.
   * 
   * @param string $name
   * @param string $content
   */
  protected function block($name, $content=null)
  {
    if (!is_null($content))
    {
      // If this is an extending template and the block
      // is empty...
      if ($this->extension && empty($this->blocks[$name]))
      {
        $this->blocks[$name] = $content;
      }
      // If this is the parent template and the block isn't
      // empty...
      elseif (!$this->extension && !empty($this->blocks[$name]))
      {
        echo $this->blocks[$name];
      }
      // If this is a parent template and there's nothing
      // in the block, echo the content.
      elseif (empty($this->blocks[$name]))
      {
        echo $content;
      }
    }
    else
    {
      if ($this->current_block)
      {
        throw new LogicException('You cannot have a block inside a block');
      }

      $this->current_block = $name;

      ob_start();
    }
  }

  /**
   * This is used to stop capturing a block's content.
   * 
   */
  protected function endBlock()
  {
    if (!$this->current_block)
    {
      throw new LogicException('Trying to end a block that hasn\'t started.');
    }

    if ($this->extension && empty($this->blocks[$this->current_block]))
    {
      $this->blocks[$this->current_block] = ob_get_clean();
    }
    elseif (!$this->extension && !empty($this->blocks[$this->current_block]))
    {
      echo $this->blocks[$this->current_block];
    }
    elseif (!$this->extension)
    {
      echo ob_get_clean();
    }

    $this->current_block = false;
  }

  /**
   * End this template on to another.
   * 
   * @param string $template
   */
  protected function extend($template)
  {
    $this->extension = $template;
  }

  /**
   * Includes another template at this point.
   * 
   * @param string $__template
   * @param mixed[] $__vars 
   */
  protected function incl($__template, array $__vars=array())
  {
    foreach (Configuration::getInstance()->get('template_dirs') as $__template_dir)
    {
      $__path = $__template_dir.DIRECTORY_SEPARATOR.$__template;
      if (is_file($__path))
      {
        extract($__vars);
        include $__path;
        break;
      }
    }
  }

  /**
   * Renders a template.
   * 
   * @param string $__template
   * @param mixed[] $__vars
   * @return string
   */
  public function render($__template, array $__vars=array())
  {
    foreach ($this->controller->getConfiguration()->template_dirs as $__template_dir)
    {
      $__path = $__template_dir.DIRECTORY_SEPARATOR.$__template;

      if (is_file($__path))
      {
        ob_start();
        extract($__vars);
        include $__path;
        $__content = ob_get_clean();

        if ($this->extension)
        {
          $__class     = get_class($this);
          $__extension = new $__class($this->controller, $this->blocks);
          $__output    = $__extension->render($this->extension);
        }
        else
        {
          $__output = $__content;
        }

        if ($this->current_block)
        {
          throw new LogicException("Block \"{$this->current_block}\" is not finished.");
        }

        return $__output;
      }
    }

    throw new Exception("Template '$template' does not exist.");
  }

  /**
   * Returns a URL based on a module, a view and params.
   * 
   * @param string $module
   * @param string $view
   * @param mixed[] $params
   * @return string
   */
  public function url($module, $view, array $params=array())
  {
    return $this->controller->getRouting()->url($module, $view, $params);
  }

  /**
   * Depicts whether the given module
   * 
   * @param type $module
   * @param type $view
   * @param array $params
   * @return boolean
   */
  public function isRoute($module, $view, array $params=array())
  {
    return $this->controller->isRoute($module, $view, $params);
  }
}

