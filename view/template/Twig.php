<?php

namespace jw\view\template;

use Configuration;
use jw\controller\Base as Controller;
use jw\response\Base as Response;
use Twig_Autoloader;
use Twig_Loader_Filesystem;
use Twig_Environment;

require_once 'Twig/Autoloader.php';
Twig_Autoloader::register();

class Twig extends Base
{
  private $twig;
  private $controller;

  public function __construct(Controller $controller)
  {
    parent::__construct($controller);

    $loader     = new Twig_Loader_Filesystem($this->controller->getConfiguration()->template_dirs);
    $this->twig = new Twig_Environment($loader);
  }

  public function render($template, array $params=array())
  {
    return $this->twig->render($template, $params);
  }
}

