<?php

namespace jw\view\template\mixin;

use jw\mixin\Base;

/**
 * A helper class for template engines.
 *
 * @author John Wright <johngeorge.wright@gmail.com>
 * @package jw.view.mixin
 */
class Url extends Base
{
  /**
   * Returns a URL based on a module, a view and params.
   * 
   * @param string $module
   * @param string $view
   * @param mixed[] $params
   * @return string
   */
  public function url($module, $view, array $params=array())
  {
    return $this->host->getController()->getRouting()->url($module, $view, $params);
  }

  /**
   * Depicts whether the given module
   * 
   * @param type $module
   * @param type $view
   * @param array $params
   * @return boolean
   */
  public function isRoute($module, $view, array $params=array())
  {
    return $this->host->getController()->isRoute($module, $view, $params);
  }
}

