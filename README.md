JW
==

JW is a modular PHP framework.

Basic example of a VC site (view and controller)
------------------------------------------------

Clone the framework into a vendor directory

```console
$ mkdir my_project && cd my_project
$ mkdir vendor
$ git clone git@bitbucket.org:johngeorgewright/jw.git vendor/jw
```

Create the directory structure

```console
$ mkdir public
$ mkdir templates
```

Bootstrap the framework (use the built in controller):

```php
<?php

// public/index.php

require_once '../vendor/jw/util/Autoloader.php';

use jw\util\Autoloader;
use jw\controller\Base as Controller;

Autoloader::register();
Autoloader::registerDir(realpath('../'));

$controller = new Controller(Configuration::getInstance());
$controller->dispatch();
```

Route the controller:

```php
<?php

// Routing.php

use jw\routing\Base;

class Routing extends Base
{
  protected function configure()
  {
    $this->add('~^/$~', 'info', 'home');
  }
}
```

Now the `/` path points to the info#home view. There are 2 parts to the view, the logic and the template. Let's first write the logic:

```php
<?php

// info/View.php

<?php

namespace info;

use jw\view\Base;
use jw\view\HttpShortcuts;
use jw\request\Http as HttpRequest;

class View extends Base
{
  protected function mixins()
  {
    $this->mixin(new HttpShortcuts($this));
  }

  public function home(HttpRequest $request)
  {
    return $this->renderTemplate('info/home.php', array(
      'title' => 'My Site'
    ));
  }
}
```

... now the template:

```php
<?php // templates/info/home.php ?>

<?php $this->extends('base.php') ?>

<?php $this->block('page_title', $title) ?>

<?php $this->block('content') ?>
  <h1><?php echo $this->title ?></h1>
  <p>Done</p>
<?php $this->endBlock() ?>
```

```php
<?php // templates/base.php ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title><?php $this->block('page_title', 'Default title') ?></title>
  </head>
  <body>
    <?php $this->block('content') ?>
    <?php $this->endBlock() ?>
  </body>
</head>
```

There's LOADS more... but that's all I'm write about. Figure the rest out for yourselves... it's the best way to learn.

