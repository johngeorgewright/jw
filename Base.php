<?php

namespace jw;

use BadMethodCallException;
use InvalidArgumentException;
use jw\mixin\Base as Mixin;

/**
 * The abstract to all jw classes.
 *
 * @author John Wright <johngeorge.wright@gmail.com>
 * @package jw
 */
abstract class Base
{
  /**
   * Mixed in objects.
   *
   * @var jw\mixin\Base[]
   */
  private $mixins=array();

  /**
   * When a function is called that doesn't directly
   * exist in this class, we check to see if the method
   * exists in the mixins.
   *
   * @param string $name
   * @param mixed[] $args
   * @return mixed
   */
  public function __call($name, array $args=array())
  {
    if (class_exists('jw\mixin\Base'))
    {
      foreach ($this->mixins as $mixin)
      {
        if (is_callable(array($mixin, $name)))
        {
          return call_user_func_array(array($mixin, $name), $args);
        }
      }
    }

    throw new BadMethodCallException('Method '.get_class($this)."::$name() does not exist.");
  }

  /**
   * The constructor runs a template.
   *
   * $this->mixins();
   *
   */
  protected function __construct()
  {
    if (class_exists('jw\mixin\Base'))
    {
      $this->mixins();
    }
  }

  /**
   * If a property doesn't exist... try the mixins.
   *
   * @param string $name
   * @return mixed
   */
  public function __get($name)
  {
    if (class_exists('jw\mixin\Base'))
    {
      foreach ($this->mixins as $mixin)
      {
        if (property_exists($mixin, $name))
        {
          return $mixin->$name;
        }
      }

      foreach ($this->mixins as $mixin)
      {
        if (is_callable(array($mixin, '__get')))
        {
          return $mixin->__get($name);
        }
      }
    }

    return $this->$name;
  }

  /**
   * If a property doesn't exist, try the mixins.
   *
   * @param string $name
   * @param mixed $value
   */
  public function __set($name, $value)
  {
    if (class_exists('jw\mixin\Base'))
    {
      foreach ($this->mixins as $mixin)
      {
        if (property_exists($mixin, $name))
        {
          $mixin->$name = $value;
          return $this;
        }
      }

      foreach ($this->mixins as $mixin)
      {
        if (is_callable(array($mixin, '__set')))
        {
          $mixin->__set($name, $value);
          return $this;
        }
      }
    }

    $this->$name = $value;
  }

  /**
   * Mixin an object.
   *
   * @param jw\Mixin $mixin
   */
  public function mixin(Mixin $mixin)
  {
    if (class_exists('jw\mixin\Base'))
    {
      if (!in_array($mixin, $this->mixins))
      {
        $this->mixins[] = $mixin;
      }
    }
  }

  protected function mixins()
  {}
}

