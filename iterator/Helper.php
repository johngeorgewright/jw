<?php

namespace jw\iterator;

use InvalidArgumentException;
use OutOfBoundsException;
use OutOfRangeException;
use Iterator;
use IteratorAggregate;

/**
 * Extra methods for PHP arrays.
 *
 * @package jw\array
 * @author John Wright <johngeorge.wright@gmail.com>
 */
abstract class Helper
{
  /**
   * Validates if the 1st parameter is iterable and the 2nd
   * is a valid and not malformed key.
   *
   * @param array|Iterator|IteratorAggregate $arr
   * @param mixed $key
   * @throws InvalidArgumentException
   * @throws OutOfRangeException
   * @thorws OutOfBoundsException
   * @return void
   */
  public static function validate($arr, $key)
  {
    if (!self::isIterable($arr))
    {
      throw new InvalidArgumentException('Expecting an array, Iterator or IteratorAggregate argument.');
    }
    elseif (!is_string($key) && !is_integer($key))
    {
      throw new OutOfRangeException('Invalid key.');
    }
    elseif (!isset($arr[$key]))
    {
      throw new OutOfBoundsException("Key \"$key\" does not exist.");
    }
  }

  /**
   * Unsets the key from the array, but also returns
   * the value.
   *
   * @param mixed[] $arr
   * @param mixed $key
   * @return mixed
   */
  public static function remove(&$arr, $key)
  {
    self::validate($arr, $key);
    $val = $arr[$key];
    unset($arr[$key]);
    return $val;
  }

  /**
   * Returns an offset's value.
   *
   * Usefull in the following situation:
   * <code>
   * echo Helper::getOffset($some->value(), 'a', 'nother', 'key');
   * // Is the same as:
   * $arr=$some->value(); echo $arr['a']['nother']['key'];
   * // or (if it were legal):
   * echo $some->value()['a']['nother']['key'];
   * </code>
   *
   * Another really useful point to this method is best
   * explained in an example.
   * <code>
   * echo Helper::getOffset($arr1, $arr2, $key);
   * // Is the same as:
   * echo $arr1[$arr2[$key]];
   * </code>
   * This makes things look a little tidier... expecially when the
   * chain is large.
   *
   * @param mixed[] $arr
   * @param mixed $key
   * @param mixed $key...
   * @return mixed
   */
  public static function getOffset($arr, $key)
  {
    $args      = array_slice(func_get_args(), 2);
    $user_func = array('jw\iterator\Helper', 'getOffset');

    if (self::isIterable($key))
    {
      $key_args          = array();
      $key_args_complete = false;
      
      foreach ($args as $arg)
      {
        if ($key_args_complete)
        {
          break;
        }
        
        $key_args[] = $arg;
        
        if (!self::isIterable($arg))
        {
          $key_args_complete = true;
        }
      }
      
      if (count($key_args))
      {
        $key  = call_user_func_array($user_func, array_merge(array($key), $key_args));
        $args = array_slice($args, count($key_args));
      }
      else
      {
        throw new InvalidArgumentException('Cannot create a key from any argument.');
      }
    }

    self::validate($arr, $key);
    $arr = $arr[$key];

    if (count($args) && self::isIterable($arr))
    {
      return call_user_func_array($user_func, array_merge(array($arr), $args));
    }

    return $arr;
  }

  /**
   * Depicts whether the given value is iterable
   * (array, Iterator or IteratorAggregate).
   *
   * @param mixed $val
   * @return boolean
   */
  public static function isIterable($val)
  {
    return is_array($val) || $val instanceof Iterator || $val instanceof IteratorAggregate;
  }
}

