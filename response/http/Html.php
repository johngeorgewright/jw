<?php

namespace jw\response\http;

use jw\response\Http;

class Html extends Http
{
  protected $content_type = 'text/html';
}

