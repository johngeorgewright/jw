<?php

namespace jw\response;

use jw\response\Base;

abstract class Http extends Base
{
  protected $status_code;
  protected $status_message;

  public function __construct($string, $status_code=200, $status_message='OK')
  {
    parent::__construct($string);
    $this->status_code = $status_code;
    $this->status_message = $status_message;
  }

  public function getStatusCode()
  {
    return $this->status_code;
  }

  public function getStatusMessage()
  {
    return $this->status_message;
  }

  public function getStatus()
  {
    return 'HTTP/1.0 '.$this->status_code.' '.$this->status_message;
  }

  public function setStatusCode($code)
  {
    // @TODO validate
    $this->status_code = $code;
    return $this;
  }

  public function setStatusMessage($message)
  {
    $this->status_message = $message;
    return $this;
  }

  public function toString()
  {
    header($this->getStatus(), true, $this->status_code);
    return parent::toString();
  }
}

