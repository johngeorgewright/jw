<?php

namespace jw\response;

use jw\Base as JwBase;

/**
 * A class representing a response.
 *
 * @author John Wright <johngeorge.wright@gmail.com>
 * @package jw.response
 */
abstract class Base extends JwBase
{
  protected $string = '';
  protected $content_type;

  /**
   * The constructor
   *
   * @param string $string
   */
  public function __construct($string)
  {
    $this->string = $string;
  }

  /**
   * The string parser.
   *
   * @return string
   */
  public function __toString()
  {
    return $this->toString();
  }

  public function getContentType()
  {
    return $this->content_type;
  }

  /**
   * The string parse.
   *
   * @return string
   */
  public function toString()
  {
    header('Content-type: '.$this->content_type);
    return $this->string;
  }
}

