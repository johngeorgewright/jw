<?php

namespace jw\controller;

use jw\util\Autoloader;
use jw\Base as JwBase;
use jw\configuration\Base as Configuration;
use jw\request\Base as Request;
use jw\request\Http as HttpRequest;
use jw\response\Base as Response;
use Exception;
use jw\exception\Display as ExceptionDisplay;
use jw\exception\Error404;
use jw\exception\Logic as LogicException;
use jw\exception\OutOfRange as OutOfRangeException;
use jw\response\http\Html;

/**
 * The Controller part of the MVC.
 *
 * @author John Wright <johngeorge.wright@gmail.com>
 * @package jw
 */
class Base extends JwBase
{
  /**
   * The configuration object.
   *
   * @var jw\Configuration
   */
  protected $configuration;

  /**
   * The request object.
   *
   * @var jw\request\Base
   */
  protected $request;

  /**
   * The routing object.
   *
   * @var jw\routing\List
   */
  protected $routing;

  /**
   * The current route.
   *
   * @var jw\Route
   */
  protected $current_route;

  /**
   * The constructor.
   *
   * @param jw\configuration\Base $configuration
   * @param mixed[] $settings
   */
  public function __construct(Configuration $configuration)
  {
    set_exception_handler(array($this, 'exceptionHandler'));
    
    $this->configuration = $configuration;
    $this->request       = Request::factory();
    $this->routing       = new \Routing();
    $this->current_route = $this->routing->matchByUrl($this->request->getUri());

    try
    {
      if ($this->current_route)
      {
        $this->request
        ->set('module', $this->current_route->module)
        ->set('action', $this->current_route->action)
        ->mSet($this->current_route->parameters);
      }
      else
      {
        throw new Error404();
      }
    }
    catch (Exception $e)
    {
      $e_display = new ExceptionDisplay($e);
      $e_display->render();
      exit;
    }
  }

  public function getConfiguration()
  {
    return $this->configuration;
  }

  public function getRouting()
  {
    return $this->routing;
  }

  /**
   * Dispatches the framework.
   *
   */
  public function dispatch()
  {
    $modules    = $this->configuration->get('enabled_modules', array());
    $request    = $this->request;
    $module     = $request->get('module');
    $view_class = $module.'\\View';
    $action     = $request->get('action');

    if (!in_array($module, $modules))
    {
      throw new LogicException("The module \"$module\" is not enabled.");
    }

    if (!class_exists($view_class))
    {
      throw new LogicException("View class \"$view_class\" does not exist.");
    }

    $view = new $view_class($this, $this->configuration->get('template_class', 'jw\view\template\Twig'));

    if (method_exists($view, 'pre'))
    {
      $view->pre($request);
    }

    $response = $view->$action($request);

    if (!$response instanceof Response)
    {
      throw new LogicException("Method $view_class->$action() should return a jw\\Response object.");
    }

    if (method_exists($view, 'post'))
    {
      $view->post($post);
    }

    echo $response;
  }

  public function isRoute($module, $action, array $params=array())
  {
    return $this->request->getUri() == $this->routing->url($module, $action, $params);
  }
  
  public function exceptionHandler(Exception $e)
  {
    $display = new ExceptionDisplay($e);
    $display->render($this->request instanceof HttpRequest);
  }
}
