<?php

namespace jw\routing;

class Route extends \jw\Base
{
  public $pattern;
  public $module;
  public $action;
  public $parameters=array();

  public function __construct($pattern, $module, $action)
  {
    $this->pattern = $pattern;
    $this->module  = $module;
    $this->action  = $action;
  }
}

