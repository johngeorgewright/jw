<?php

namespace jw\routing;

use jw\Base as JwBase;
use jw\Request;
use jw\routing\Route;

/**
 * The routing abstract.
 *
 * @author John Wright <johngeorge.wright@gmail.com>
 * @package jw
 */
abstract class Base extends JwBase
{
  /**
   * An array of configured routes.
   *
   * @var jw\Route[]
   */
  protected $routes=array();

  /**
   * The constructor requires a request to match against.
   *
   * @param jw\Request
   */
  public function __construct()
  {
    $this->configure();
  }

  /**
   * Searches the configured routes and matches the current
   * one in the request.
   *
   * @param string $url
   * @return jw\Route
   */
  public function matchByUrl($url)
  {
    foreach ($this->routes as $pattern => $route)
    {
      if (preg_match($pattern, $url, $matches))
      {
        foreach ($matches as $key => $value) {
          if (is_string($key)) {
            $route->parameters[$key] = $value;
          }
        }

        return $route;
      }
    }

    return false;
  }

  public function matchByView($module, $action)
  {
    foreach ($this->routes as $route)
    {
      if ($route->module == $module && $route->action == $action)
      {
        return $route;
      }
    }

    return false;
  }

  /**
   * Adds a route to the configuration.
   *
   * @param string $url
   * @param string $module
   * @param string $action
   * @return void
   */
  protected function add($url, $module, $action)
  {
    $this->routes[$url] = new Route($url, $module, $action);
  }

  public function url($module, $view, array $params=array())
  {
    $route = $this->matchByView($module, $view);

    if (!$route)
    {
      throw new OutOfRangeException("The view \"$module/$view\" does not exist.");
    }

    $regex_delim = substr($route->pattern, 0, 1);
    $url_end_pos = strrpos($route->pattern, $regex_delim);
    $url         = substr($route->pattern, 1, $url_end_pos-1);
    $url         = preg_replace(array('/^\^/', '/\$$/'), '', $url);
     
    return $url;
  }

  /**
   * The configuration of routes.
   *
   */
  abstract protected function configure();
}

