<?php

namespace jw\util;

/**
 * A class loader.
 *
 * Works by replacing the namespace separator with
 * a directory separator. IE this will look for the
 * class "jw\response\http\Html" in "jw/response/http/Html.php".
 *
 * If you'd like to register a directory for this
 * autoloader to work with, use the Autloader::registerDir()
 * method.
 *
 * @author John Wright<johngeorge.wright@gmail.com>
 * @package jw
 */
class Autoloader
{
  /**
   * The directory which to register autoloading from.
   *
   * @var string
   */
  private $root_dir;

  /**
   * @param string $root_dir The directory which to register autoloading from.
   */
  public function __construct($root_dir)
  {
    $this->root_dir = $root_dir;
  }

  /**
   * Registers the autoloader with the jw directory.
   *
   */
  public static function register()
  {
    self::registerDir(realpath(
    dirname(__FILE__).DIRECTORY_SEPARATOR.
      '..'.DIRECTORY_SEPARATOR.
      '..'
    ));
  }

  public static function registerDir($dir)
  {
    $autoloader = new self($dir);
    spl_autoload_register(array($autoloader, 'loadClass'));
  }

  /**
   * The autoloading method.
   *
   * @param string $class_name
   */
  public function loadClass($class_name)
  {
    $file = $this->root_dir.DIRECTORY_SEPARATOR
    .Autoloader::getRelativeClassPath($class_name);

    if (is_file($file))
    {
      require_once $file;
    }
  }

  public static function getRelativeClassPath($class_name)
  {
    return str_replace('\\', DIRECTORY_SEPARATOR, $class_name).'.php';
  }
}

