<?php

namespace jw\var_holder;

use jw\Base as JwBase;
use Iterator;
use InvalidArgumentException;

/**
 * An class to hold a number of random variables.
 *
 * @author John Wright <johngeorge.wright@gmail.com>
 * @package jw
 */
class Base extends JwBase implements I, Iterator
{
  /**
   * The variables
   *
   * @var mixed[]
   */
  private $_vars=array();
  
  /**
   * Position of the iteration.
   * 
   * @var mixed
   */
  private $_position=0;

  /**
   * The constructor accepts a key/value array
   * of variables.
   *
   * @param mixed[] $vars
   */
  public function __construct(array $vars=array())
  {
    $this->_vars = $vars;
  }

  /**
   * @see jw\var_holder\I
   */
  public function __get($name)
  {
    return $this->get($name);
  }

  /**
   * @see jw\var_holder\I
   */
  public function __set($name, $value)
  {
    $this->set($name, $value);
  }
  
  /**
   * @see Iterator
   */
  public function current()
  {
    return $this->_vars[$this->_position];
  }

  /**
   * @see jw\var_holder\I
   */
  public function get($name, $default=null)
  {
    return array_key_exists($name, $this->_vars) ? $this->_vars[$name] : $default;
  }

  /**
   * @see jw\var_holder\I
   */
  public function getAll()
  {
    return $this->_vars;
  }

  /**
   * @see jw\var_holder\I
   */
  public function has($name)
  {
    return array_key_exists($name, $this->_vars);
  }
  
  /**
   * @see Iterator
   */
  public function key()
  {
    $arr = array_flip($this->_vars);
    return $arr[$this->_position];
  }

  /**
   * @see jw\var_holder\I
   */
  public function mSet(array $values)
  {
    $this->_vars = array_merge($this->_vars, $values);
    return $this;
  }
  
  /**
   * @see Iterator
   */
  public function next()
  {
    return $this->_vars[++$this->_position];
  }
  
  /**
   * @see Iterator
   */
  public function rewind()
  {
    $this->_position = 0;
    return $this->current();
  }

  /**
   * @see jw\var_holder\I
   */
  public function set($name, $value)
  {
    $this->_vars[$name] = $value;
    return $this;
  }
  
  /**
   * @see Iterator
   */
  public function valid()
  {
    return array_key_exists($this->key(), $this->_vars);
  }
}

