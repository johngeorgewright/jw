<?php

namespace jw\var_holder;

use jw\Base as JwBase;
use jw\mixin\Base;
use jw\var_holder\Base as VarHolder;

class Mixin extends Base implements I
{
  private $var_holder;
  
  public function __construct(JwBase $host, array $vars=array())
  {
    parent::__construct($host);
    $this->var_holder = new VarHolder($vars);
  }
  
  public function __get($name)
  {
    return $this->var_holder->$name;
  }
  
  public function __set($name, $value)
  {
    $this->var_holder->$name = $value;
  }
  
  public function get($name, $default=null)
  {
    return $this->var_holder->get($name, $default);
  }
  
  public function getAll()
  {
    return $this->var_holder->getAll();
  }
  
  public function has($name)
  {
    return $this->var_holder->has($name);
  }
  
  public function set($name, $value)
  {
    return $this->var_holder->set($name, $value);
  }
  
  public function mSet(array $values)
  {
    $this->var_holder->mSet($values);
  }
}
