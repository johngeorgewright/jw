<?php

namespace jw\var_holder;

interface I
{
  /**
   * If the returning parameter does not exit
   * use get()
   *
   * @param string $name
   * @return mixed
   */
  public function __get($name);
  
  /**
   * If setting a parameter that does not
   * exist use set()
   *
   * @param string $name
   * @param mixed $value
   */
  public function __set($name, $value);
  
  /**
   * Returns the value to the key ($name).
   * If the key does not exist, return
   * the passed default value.
   *
   * @param string $name
   * @param mixed $default
   * @return mixed
   */
  public function get($name, $default=null);
  
  /**
   * Returns the key/value array of variables.
   *
   * @return mixed[]
   */
  public function getAll();
  
  /**
   * Returns a boolean value depicting whether
   * the key exists.
   *
   * @param string $name
   * @return boolean
   */
  public function has($name);
  
  /**
   * Multiple-set.
   *
   * Adds an array of key/value variables.
   *
   * @param mixed[]
   * @return jw\var_holder\I
   */
  public function mSet(array $values);
  
  /**
   * Sets a variable.
   *
   * @param string $name
   * @param mixed $value
   * @return jw\var_holder\I
   */
  public function set($name, $value);
}
