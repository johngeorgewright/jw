<?php

namespace jw\configuration;

use jw\singleton\Base as Singleton;
use jw\var_holder\Mixin as VarHolder;

/**
 * A configuration abstract.
 *
 * Simply a singleton given the name "Configuration".
 * Handy way to pass configuration variables around
 * a framework.
 *
 * @author John Wright <johngeorge.wright@gmail.com>
 * @package jw
 */
abstract class Base extends Singleton
{
  protected function __construct()
  {
    parent::__construct();
    $this->configure();
  }

  abstract protected function configure();

  protected function mixins()
  {
    $this->mixin(new VarHolder($this));
  }
}

