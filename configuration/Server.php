<?php

namespace jw\configuration;

use jw\var_holder\Mixin as VarHolder;

class Server extends Base
{
  protected function mixins()
  {
    $this->mixin(new VarHolder($this, $_SERVER));
  }
}
