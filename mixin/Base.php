<?php

namespace jw\mixin;

use jw\Base as JwBase;

/**
 * An interface for mixins.
 *
 * Any class that you wish to mixin to another
 * must implement this class.
 *
 * @author John Wright <johngeorge.wright@gmail.com>
 * @package jw
 */
abstract class Base extends JwBase
{
  protected $host;

  public function __construct(JwBase $host)
  {
    $this->host = $host;
  }
}

