<?php

namespace jw\request;

use jw\request\Base;

class Http extends Base
{
  public function __construct()
  {
    parent::__construct($_REQUEST);
  }

  public function getHost()
  {
    return $_SERVER['HTTP_HOST'];
  }

  public function getProtocol()
  {
    return $_SERVER['HTTPS'] ? 'https' : 'http';
  }

  public function getUri()
  {
    return $_SERVER['REQUEST_URI'];
  }
}

