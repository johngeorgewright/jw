<?php

namespace jw\request;

use jw\var_holder\Base as VarHolder;
use jw\request\Http;
use jw\request\Cli;
use jw\Exception;

/**
 * A class to represent an incoming request.
 *
 * @author John Wright <johngeorge.wright@gmail.com>
 * @package jw
 */
abstract class Base extends VarHolder
{
  public static function factory()
  {
    if (array_key_exists('HTTP_HOST', $_SERVER))
    {
      return new Http();
    }
    elseif (array_key_exists('argv', $_SERVER))
    {
      return new Cli();
    }
    else
    {
      throw new Exception('Cannot decipher request type.');
    }
    exit;
  }
}

