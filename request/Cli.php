<?php

namespace jw\request;

use jw\request\Base;

class Cli extends Base
{
  public function __construct()
  {
    parent::__construct($_SERVER['argv']);
  }
}

